# -*- coding: utf-8 -*-
#
# Syntax-highlight code automatically.
# (this script requires Python >= 2.6)
#

SCRIPT_NAME    = 'pygments-weechat'
SCRIPT_AUTHOR  = 'Till Hoeppner <till@hoeppner.ws>'
SCRIPT_VERSION = '0.0.1'
SCRIPT_LICENSE = 'GPL3'
SCRIPT_DESC    = 'Automatically syntax-highlight code inline.'
CHARSET = 'UTF-8'

SCRIPT_COMMAND = 'pygments'
SCRIPT_BUFFER  = 'pygments'

import_ok = True

try:
    import weechat
except ImportError:
    print('This script must be run under WeeChat.')
    print('Get WeeChat now at: http://www.weechat.org/')
    import_ok = False

try:
    import sys, os, string, subprocess, re
except ImportError as message:
    print('Missing package(s) for %s: %s' % (SCRIPT_NAME, message))
    import_ok = False

OPTIONS = {
        'pygmentize_path': (os.path.expanduser('~/code/python/pygments-irc/pygmentize'), 'The pygmentize version to call for highlighting.'),
        'colorscheme': ('default', 'The colorscheme to use for highlighting.'),
        'delimiter': ('`', 'The delimiter to indicate code snippets.'),
        'deflang': ('', 'The default language per channel, as (chan:lang,)*')
}

BEFORE  = r'(?:(?P<lang>\w+){delim}(?P<code>.*?){delim})'
AFTER   = r'(?:{delim}(?P<code>.*?){delim}(?P<lang>\w+))'
DEFAULT = r'(?:{delim}(?P<code>.*?){delim})'

def unescape(s):
    return s.replace(r'\{}'.format(OPTIONS['delimiter']), OPTIONS['delimiter'])

def modify(data, modifier, modifier_data, string):
    buffer = weechat.current_buffer()
    name = weechat.buffer_get_string(buffer, "name")
    if name.startswith('server.') or \
       string.startswith('/buffer') or \
       string.startswith('/input'): return string
    # enable escaping of the delimiter
    escape = r'(?:(?<!\\){})'.format(OPTIONS['delimiter'])
    for p in [BEFORE.format(delim = escape), AFTER.format(delim = escape), DEFAULT.format(delim = escape)]:
        m = re.search(p, string)
        while m:
            code_span = m.span('code')
            try: lang_span = m.span('lang')
            except IndexError: lang_span = None
            code = string[code_span[0]:code_span[1]]
            if lang_span:
                lang = string[lang_span[0]:lang_span[1]]
            else:
                r = re.search('{chan}:(?P<lang>\w+),'.format(chan = name), OPTIONS['deflang'])
                if r:
                    lang = r.group('lang')
                else:
                    weechat.prnt('', 'Unable to determine language. Bailing out.')
                    return unescape(string)
            if lang_span and code_span[0] < lang_span[0]:
                before = string[:code_span[0] - 1]
                after = string[lang_span[1] + 1:]
            elif lang_span and code_span[0] >= lang_span[0]:
                before = string[:lang_span[0]]
                after = string[code_span[1] + 1:]
            else:
                before = string[:code_span[0] - 1]
                after = string[code_span[1] + 1:]

            cmd = [
                OPTIONS['pygmentize_path'],
                '-f', 'irc',
                '-l', lang,
                '-O', 'style={}'.format(OPTIONS['colorscheme'])
            ]
            process = subprocess.Popen(cmd,
                        stdin = subprocess.PIPE,
                        stdout = subprocess.PIPE,
                        stderr = subprocess.STDOUT)
            output = process.communicate(input = code)[0].rstrip("\r\n")

            string = before + output + after
            m = re.search(p, string)

    return unescape(string)

def config_change(data, option, value):
    reload_settings()
    return weechat.WEECHAT_RC_OK

def reload_settings():
    for option, value in OPTIONS.items():
        if weechat.config_is_set_plugin(option):
            OPTIONS[option] = weechat.config_get_plugin(option)

if __name__ == '__main__' and import_ok:
    if weechat.register(SCRIPT_NAME, SCRIPT_AUTHOR, SCRIPT_VERSION, SCRIPT_LICENSE,
                        SCRIPT_DESC, u'', u'UTF-8'):
        # set default settings
        version = weechat.info_get('version_number', '') or 0
        for option, value in OPTIONS.items():
            if not weechat.config_is_set_plugin(option):
                weechat.config_set_plugin(option, value[0])
                OPTIONS[option] = value[0]
            if int(version) >= 0x00030500:
                weechat.config_set_desc_plugin(option, '%s (default: "%s")' % (value[1], value[0]))
        reload_settings()

        weechat.hook_modifier('input_text_for_buffer', 'modify', '')
        weechat.hook_config('plugins.var.python.' + SCRIPT_NAME + '.*', 'config_change', '')
